'use strict';

const
    config = require('config'),
    requestPromise = require('request-promise'),
    redis = require('redis'),
    redisConfig = config.get('redis'),
    redisConnectionConfig = config.get('redisConnection'),
    redisClient = redis.createClient(redisConnectionConfig),
    gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    gulpSequence = require('gulp-sequence'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    jscs = require('gulp-jscs'),
    mocha = require('gulp-mocha'),
    bluebird = require('bluebird'),
    paths = {
        jsCode: [
            'controllers/**/*.js',
            'models/**/*.js',
            'routes/**/*.js',
            'tests/*.js',
            'index.js'
        ]
    };

// Add promises to node-redis library
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

// Check code formatting
gulp.task('jshint-jscs', function () {
    return gulp.src(paths.jsCode)
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('jshint-stylish'), { verbose: true })
        .pipe(jscs())
        .pipe(jscs.reporter());
});

// Fetch default books from google
gulp.task('fetch-google-books', function () {
    console.log('\n\nFetching books from Google ...');
    return requestPromise.get(config.get('googleBooksUrl')).then(function (searchResult) {
        searchResult = JSON.parse(searchResult);
        const books = searchResult.items;

        // Save books in redis, also storing a separate hash index
        books.forEach(function (book, index) {
            redisClient.rpush(redisConfig.listName, JSON.stringify(book));
            redisClient.hset(redisConfig.hashIndexName, book.id, index);
        });
        console.log(`\n\n${books.length} books stored\n\n`);
    }).catch(function () {
        console.log(`\n\nCannot connect to the Internet, please check your connection and run gulp again\n\n`);
    });
});

gulp.task('watch', function () {
    gulp.watch(paths.jsCode, ['jshint-jscs']);
});

gulp.task('nodemon', function () {
    nodemon({
        exec: 'node',
        script: 'index.js',
        ext: 'js json',
        ignore: ['.idea/**/*', 'tests/**/*', 'scripts/**/*'],
        //verbose: true
        quiet: true
    }).on('restart', function () {
        console.log('\n\ngulp process restarted!\n\n');
    });
});

gulp.task('tests', function () {
    return gulp.src(['tests/*.js'], { read: false })
        .pipe(mocha({ reporter: 'list' }))
        .on('error', gutil.log)
        .once('end', function () {
            process.exit();
        });
});

gulp.task('default', gulpSequence(['fetch-google-books'], ['jshint-jscs'], ['watch'], ['nodemon']));
