'use strict';

const
    config = require('config'),
    redis = require('redis'),
    bcrypt = require('bcryptjs'),
    redisConnectionConfig = config.get('redisConnection'),
    redisClient = redis.createClient(redisConnectionConfig),
    jwt = require('jsonwebtoken'),

    User = function User (email) {
        this.email = email;
    };

User.prototype = {
    // Check if user already exists in DB
    exists: function () {
        const email = this.email;
        return redisClient.hlenAsync(`greentube:users:${this.email}`).then(function (res) {
            return res === 1;
        });
    },

    // Save user in DB
    save: function (password) {
        const email = this.email;

        return new Promise(function (resolve, reject) {
            // Encrypt password
            bcrypt.genSalt(10, function (err, salt) {
                if (err) {
                    reject(err);
                }
                bcrypt.hash(password, salt, function (err, hash) {
                    if (err) {
                        reject(err);
                    }
                    redisClient.hsetAsync(`greentube:users:${email}`, 'hashedPassword', hash).then(function (res) {
                        resolve();
                    });
                });
            });
        });
    },

    // Authenticate user given it's password
    authenticate: function (password) {
        const tokenPayload = { email: this.email };
        return redisClient.hgetAsync(`greentube:users:${this.email}`, 'hashedPassword').then(function (hashedPassword) {
            return new Promise(function (resolve, reject) {
                if (hashedPassword !== null && bcrypt.compareSync(password, hashedPassword)) {
                    // SIgn a new token
                    jwt.sign(tokenPayload, config.jwt.key, {
                        issuer: 'greentube',
                        algorithm: config.jwt.algorithm,
                        expiresIn: '2h'
                    }, function (err, token) {
                        if (!err) {
                            resolve(token);
                        } else {
                            reject();
                        }
                    });
                } else {
                    reject();
                }
            });
        });
    }
};

module.exports = User;
