'use strict';

const
    config = require('config'),
    redis = require('redis'),
    redisConfig = config.get('redis'),
    redisConnectionConfig = config.get('redisConnection'),
    redisClient = redis.createClient(redisConnectionConfig),
    Book = function Book () {};

// Fetch books from redis, given pagination options
Book.getAll = function (page = 1, limit = 10) {
    const
        start = limit * (page - 1),
        stop = (limit * page) - 1;

    return redisClient.lrangeAsync(redisConfig.listName, start, stop).then(function (res) {
        return {
            books: res.map(function (element) {
                return JSON.parse(element);
            }),
            page: page,
            limit: limit
        };
    });
};

// Get a book from redis
Book.getOne = function (id) {
    return redisClient.hgetAsync(redisConfig.hashIndexName, id).then(function (index) {
        if (index === null) {
            return null;
        }
        return redisClient.lindexAsync(redisConfig.listName, index).then(function (res) {
            return JSON.parse(res);
        });
    });
};

Book.prototype = {

};

module.exports = Book;

