'use strict';

const
    chakram = require('chakram'),
    expect = chakram.expect,
    config = require('config'),
    redis = require('redis'),
    redisConnectionConfig = config.get('redisConnection'),
    redisClient = redis.createClient(redisConnectionConfig),
    url = `http://${config.get('host')}:${config.get('port')}/users`;

describe('register', function () {
    let payload = { email: 'manuel.bitto@gmail.com', password: 'greentubeiscool' };

    it('should return an error if payload does not have email', function () {
        let wrongPayload = Object.create(payload);
        delete wrongPayload.email;
        return expect(chakram.post(url, wrongPayload)).to.have.status(400);
    });

    it('should return an error if payload does not have password', function () {
        let wrongPayload = Object.create(payload);
        delete wrongPayload.password;
        return expect(chakram.post(url, wrongPayload)).to.have.status(400);
    });

    it('should return an error if password does not have at least 6 chars', function () {
        let wrongPayload = Object.create(payload);
        wrongPayload.password = 'abcde';
        return expect(chakram.post(url, wrongPayload)).to.have.status(400);
    });

    it('should register a new user', function () {
        return expect(chakram.post(url, payload)).to.have.status(201);
    });

    it('should not register a duplicate user', function () {
        return expect(chakram.post(url, payload)).to.have.status(409);
    });

    after('delete user', function () {
        redisClient.del(`greentube:users:${payload.email}`);
    });
});
