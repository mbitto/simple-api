'use strict';

const
    User = require('../models/User'),
    chakram = require('chakram'),
    expect = chakram.expect,
    config = require('config'),
    redis = require('redis'),
    redisConnectionConfig = config.get('redisConnection'),
    redisClient = redis.createClient(redisConnectionConfig),
    url = `http://${config.get('host')}:${config.get('port')}/auth`;

describe('authenticate', function () {
    const payload = { email: 'manuel.bitto@gmail.com', password: 'greentubeiscool' };

    before('create user in DB', function () {
        const user = new User(payload.email);
        return user.save(payload.password);
    });

    it('should return an error if payload does not have email', function () {
        let wrongPayload = JSON.parse(JSON.stringify(payload));
        delete wrongPayload.email;
        return expect(chakram.post(url, wrongPayload)).to.have.status(400);
    });

    it('should return an error if payload does not have password', function () {
        let wrongPayload = JSON.parse(JSON.stringify(payload));
        delete wrongPayload.password;
        return expect(chakram.post(url, wrongPayload)).to.have.status(400);
    });

    it('should not authenticate not existing users', function () {
        let wrongPayload = JSON.parse(JSON.stringify(payload));
        wrongPayload.email = 'john@example.com';
        return expect(chakram.post(url, wrongPayload)).to.have.status(401);
    });

    it('should give back a JWT token', function () {
        return expect(chakram.post(url, payload)).to.have.status(200);
    });

    it('should not authenticate a user if wrong password is provided', function () {
        let wrongPayload = JSON.parse(JSON.stringify(payload));
        wrongPayload.password = 'abcd';
        return expect(chakram.post(url, wrongPayload)).to.have.status(401);
    });

    after('delete user', function () {
        redisClient.del(`greentube:users:${payload.email}`);
    });
});
