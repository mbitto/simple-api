'use strict';

const
    User = require('../models/User'),
    chakram = require('chakram'),
    expect = chakram.expect,
    config = require('config'),
    redis = require('redis'),
    redisConfig = config.get('redis'),
    redisConnectionConfig = config.get('redisConnection'),
    redisClient = redis.createClient(redisConnectionConfig),
    url = `http://${config.get('host')}:${config.get('port')}/books`;

describe('books', function () {
    const
        userData = { email: 'manuel.bitto@gmail.com', password: 'greentubeiscool' },
        book1 = { 'id': 'abcdefg' },
        book2 = { 'id': 'hilmnop' };

    let user,
        token;

    before('add a user in DB', function () {
        user = new User(userData.email);
        return user.save(userData.password);
    });

    before('authenticate user', function () {
        return user.authenticate(userData.password).then(function (userToken) {
            token = userToken;
            return token;
        });
    });

    before('add books in DB ', function () {
        return redisClient.delAsync(redisConfig.listName).then(function () {
            return redisClient.delAsync(redisConfig.hashIndexName);
        }).then(function () {
            return redisClient.rpushAsync(redisConfig.listName, JSON.stringify(book1));
        }).then(function () {
            return redisClient.rpushAsync(redisConfig.listName, JSON.stringify(book2));
        }).then(function () {
            return redisClient.hsetAsync(redisConfig.hashIndexName, book1.id, 0);
        }).then(function () {
            return redisClient.hsetAsync(redisConfig.hashIndexName, book2.id, 1);
        });
    });

    it('should return unauthorized if no token is attached', function () {
        return expect(chakram.get(url)).to.have.status(401);
    });

    it('should give back a result of paginated books', function () {

        const options = {
            headers: { 'Authorization': `Bearer ${token}` }
        };

        return chakram.get(url, options).then(function (response) {
            expect(response).to.have.status(200);
            expect(response.body.books).to.have.lengthOf(2);
            expect(response.body.page).to.be.at.least(1);
            expect(response.body.limit).to.be.at.least(1);
            return chakram.wait();
        });
    });

    it('should give back a single book result', function () {

        const options = {
            headers: { 'Authorization': `Bearer ${token}` }
        };

        return chakram.get(`${url}/${book1.id}`, options).then(function (response) {
            expect(response).to.have.status(200);
            expect(response.body.id).to.be.a('string');
            return chakram.wait();
        });
    });

    it('should return null if the book id is not existing', function () {

        const options = {
            headers: { 'Authorization': `Bearer ${token}` }
        };

        return chakram.get(`${url}/this-is-not-a-valid-id`, options).then(function (response) {
            expect(response).to.have.status(404);
            expect(response.body).to.equal(undefined);
            return chakram.wait();
        });
    });

    after('delete user and books', function () {
        redisClient.del(`greentube:users:${userData.email}`);
        redisClient.del(redisConfig.listName);
        redisClient.del(redisConfig.hashIndexName);
    });
});
