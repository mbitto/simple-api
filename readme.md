# Greentube Demo API

This API is a demo I made for Greentube, as a first step of their recruitment process. It's intended to serve books, initially fetched from google books API and stored locally on redis, only to authenticated users. Users can register and login through API.


How to run this app:

* install gulp globally: *$ npm i -g gulp*
* install dependencies: *$ npm i*
* run *$ gulp* from your console

About the documentation:

* http://localhost:8000/documentation to see it in your browser
* to make a request against /books you need a JWT token (you'll get it after a successful login). You have to insert the token in the input field at the top right of the screen in this format: **Bearer \<my-jwt\>**

About the configuration:

* in /config you'll find the configuration parameters used by the app.
* You may want to change redis config accordingly to your machine
* there is also a test configuration, this was intended to have a separate environment for testing. **It's not done yet, I didn't have the time**

About the tests:

* you find functional tests in /tests
* to run the tests: *$ gulp tests* **in a separate console** (be sure API process is still running at the same time)
* please note that tests will reset and/or overwrite your DB (see note above about testing configuration)
* stop and run *$ gulp* again from the first terminal window to re-fetch the books from Google

Other notes:

* Token should expire after two hours. A refresh token API would be a solution to avoid the need of re-login 
* See */backend_developer_test.pdf* for more details about the task
