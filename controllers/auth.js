'use strict';

const User = require('../models/User');

module.exports = {
    authenticateUser: function (request, reply) {

        const user = new User(request.payload.email);

        // Reply with a token if user could successfully authenticate
        user.authenticate(request.payload.password).then(function (token) {
            if (token) {
                reply(token);
            } else {
                reply().code(401);
            }
        }).catch(function () {
            reply().code(401);
        });
    }
};
