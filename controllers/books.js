'use strict';

const Book = require('../models/Book');

module.exports = {
    getBooks: function (request, reply) {
        // Get multiple books optionally controlling the pagination
        Book.getAll(request.query.page, request.query.limit).then(function (result) {
            reply({
                books: result.books,
                page: result.page,
                limit: result.limit
            });
        });
    },

    // Get a single book
    getBook: function (request, reply) {
        Book.getOne(request.params.id).then(function (res) {
            if (res === null) {
                reply().code(404);
            } else {
                reply(res);
            }
        });
    }
};
