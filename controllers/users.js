'use strict';

const User = require('../models/User');

module.exports = {
    addUser: function (request, reply) {
        let user = new User(request.payload.email);

        user.exists().then(function (exists) {
            if (exists) {
                reply('user already exists').code(409);
            } else {
                return user.save(request.payload.password).then(function () {
                    reply().code(201);
                });
            }
        });
    }
};
