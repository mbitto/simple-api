'use strict';

const
    path = require('path'),
    fs = require('fs'),
    requireHapiRoutes = require('require-hapiroutes');

fs.readdirSync(__dirname).filter(function (file) {
    return path.join(__dirname, file) != __filename && file.split('.').pop() === 'js' && file.indexOf('.') !== 0;
}).forEach(function (file) {
    const routes = require('./' + path.basename(file)).routes;
    routes.forEach(function (route, index, array) {
        route.config = route.config || {};
        route.config.plugins = route.config.plugins || {};
        route.config.plugins['hapi-swagger'] = route.config.plugins['hapi-swagger'] || {};
        route.config.plugins['hapi-swagger'].order = index + 1;
    });
});

module.exports = requireHapiRoutes(module);
