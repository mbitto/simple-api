'use strict';

const
    authController = require('../controllers/auth'),
    authValidator = require('./validators/auth');

module.exports.routes = [
    {
        method: 'POST',
        path: '/auth',
        config: {
            description: 'Authenticate a user giving back a JWT',
            notes: 'Email and password needed',
            tags: ['api'],
            auth: false,
            validate: {
                payload: authValidator.payload
            },
            handler: authController.authenticateUser
        }
    }
];
