'use strict';

const
    usersController = require('../controllers/users'),
    usersValidator = require('./validators/users');

module.exports.routes = [
    {
        method: 'POST',
        path: '/users',
        config: {
            description: 'Create a new user',
            notes: 'Email and password needed',
            tags: ['api'],
            auth: false,
            validate: {
                payload: usersValidator.payload
            },
            handler: usersController.addUser
        }
    }
];
