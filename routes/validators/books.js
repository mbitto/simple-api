'use strict';

const Joi = require('joi');

module.exports = {

    params: {
        id: Joi.string()
            .description('The book ID')
            .required()
    },

    query: {
        page: Joi.number()
            .integer()
            .min(1)
            .description('Page position of the pagination'),

        limit: Joi.number()
            .integer()
            .min(1)
            .max(20)
            .description('Maximum number of results per page')
    }
};
