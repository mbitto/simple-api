'use strict';

const Joi = require('joi');

module.exports = {

    payload: {
        email: Joi.string()
            .example('user@example.com')
            .description('Email address of the user')
            .email()
            .lowercase()
            .required(),

        password: Joi.string()
            .example('Password of the user')
            .min(6)
            .max(50)
            .description('Password of the user')
            .required()
    }
};
