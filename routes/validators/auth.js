'use strict';

const Joi = require('joi');

module.exports = {

    payload: {
        email: Joi.string()
            .example('user@example.com')
            .description('Email address of the user')
            .email()
            .lowercase()
            .required(),

        password: Joi.string()
            .example('Password of the user')
            .description('Password of the user')
            .required()
    }
};
