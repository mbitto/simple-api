'use strict';

const
    booksController = require('../controllers/books'),
    booksValidator = require('./validators/books');

module.exports.routes = [
    {
        method: 'GET',
        path: '/books',
        config: {
            description: 'Get all the books',
            notes: 'Results are paginated',
            tags: ['api'],
            // We only want authenticated users on this route
            auth: {
                strategy: 'jwt'
            },
            validate: {
                query: booksValidator.query
            },
            handler: booksController.getBooks
        }
    },

    {
        method: 'GET',
        path: '/books/{id}',
        config: {
            description: 'Get a specific book',
            notes: 'Specify the id of the book to get the single resource',
            tags: ['api'],
            // We only want authenticated users on this route
            auth: {
                strategy: 'jwt'
            },
            validate: {
                params: booksValidator.params
            },
            handler: booksController.getBook
        }
    }
];
