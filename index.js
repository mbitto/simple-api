'use strict';

const
    config = require('config'),
    bluebird = require('bluebird'),
    redis = require('redis'),
    Hapi = require('hapi'),
    Inert = require('inert'),
    Vision = require('vision'),
    blipp = require('blipp'),
    HapiSwagger = require('hapi-swagger'),
    Pack = require('./package'),
    hapiAuthJWT = require('hapi-auth-jwt'),
    server = new Hapi.Server(),
    routes = require('./routes').routes,
    jwtConfig = config.get('jwt');

// Add promises to node-redis library
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

server.connection({ host: config.get('host'), port: config.get('port') });

// Log on console http requests
server.on('response', function (request) {
    console.log(request.method.toUpperCase() + ' ' + request.url.path + ' -> ' + request.response.statusCode);
});

// Register Hapi's plugins
server.register([
    Inert,
    Vision,
    { register: blipp, options: { showAuth: true, showStart: true }},
    {
        register: HapiSwagger,
        options: {
            info: {
                title: 'Greentube Books API Documentation',
                version: Pack.version
            },
            securityDefinitions: {
                'jwt': { 'type': 'apiKey', 'name': 'Authorization', 'in': 'header' }
            }
        }
    },
    hapiAuthJWT
], function (err) {
    if (err) {
        throw err;
    }

    // Add jwt auth strategy (see hapi-auth-jwt module)
    server.auth.strategy('jwt', 'jwt', { key: jwtConfig.key, verifyOptions: { algorithms: jwtConfig.algorithm }});

    // Fetch and load all the routes
    server.route(routes);

    server.start(function (err) {

        if (err) {
            throw err;
        }
        console.log('Server running at:', server.info.uri);
    });
});

